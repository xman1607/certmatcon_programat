<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class PersonLabTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'person_lab_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'name', 
    	'function'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}