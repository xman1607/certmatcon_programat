<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class HomeSliderTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'home_slider_translate';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $timestamps = false;

}