<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class HomeBenefitsTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'home_benefits_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'title',
    	'text'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}