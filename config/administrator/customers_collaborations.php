<?php

use App\CustomerCollaborations;

return [
    'title'  => 'Colaborări',

    'description' => 'Colaborări',

    'model'  => CustomerCollaborations::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        'position',

        'logo' => column_element('', false, '<img width="100" src="(:logo)" />'),

        'title',

        'active' => column_element('Activ', false, function($row){
            return output_boolean($row);
        })
    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query->orderBy('position');
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'logo' => [
            'type' => 'image',
            'location' => 'upload/customers/collaborations'
        ],

        'title' => form_text('Titlu') + translatable(),

        'position'    => form_number($label = 'Ordinea. Numar intreg. Cu cit e mai mare, cu atit e mai jos-dreapta.', ['min' => 1, 'max' => 100000, 'required' => 'required']),

        'active' => filter_select('Activ', [
            1 => 'Da',
            0 => 'Nu'
        ]),
    ]
];