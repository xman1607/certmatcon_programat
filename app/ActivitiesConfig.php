<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class ActivitiesConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'activities_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
                                    'title_on_page'
    								];


    /**
     * @var
     */
    public $translationModel = ActivitiesConfigTranslate::class;

}