<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class CareerTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'career_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'title',
    	'text',
    	'location'
	];

    /**
     * @var bool
     */
    public $timestamps = false;

}