<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerCollaborationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_collaborations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable();
            $table->integer('position')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('customer_collaboration_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('customer_collaborations_id');
            $table->unsignedInteger('language_id');

            $table->text('title')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('customer_collaborations_id')->references('id')->on('customer_collaborations')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_collaborations');
        Schema::dropIfExists('customer_collaboration_translate');
    }
}
