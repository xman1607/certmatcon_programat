<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('activitie_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('activities_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('text')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('activities_id')->references('id')->on('activities')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
        Schema::dropIfExists('activitie_translate');
    }
}
