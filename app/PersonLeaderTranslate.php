<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class PersonLeaderTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'person_leader_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'name', 
    	'function'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}