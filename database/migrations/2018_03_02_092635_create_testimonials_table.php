<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('testimonial_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('testimonials_id');
            $table->unsignedInteger('language_id');

            $table->string('name')->nullable();
            $table->text('text')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('testimonials_id')->references('id')->on('testimonials')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
        Schema::dropIfExists('testimonial_translate');
    }
}
