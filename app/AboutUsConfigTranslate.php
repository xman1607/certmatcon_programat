<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class AboutUsConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'about_us_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
                            'title_on_page',
                            'subtitle_on_page',
                            'home_description',
                            'general_presentation',
                            'short_history',
                            'description_missions',
                            'description_concept',
                            'description_strategy'
						];

    /**
     * @var bool
     */
    public $timestamps = false;

}