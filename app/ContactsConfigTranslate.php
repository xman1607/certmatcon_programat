<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class ContactsConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'contacts_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 'meta_description', 'title_on_page'];

    /**
     * @var bool
     */
    public $timestamps = false;

}