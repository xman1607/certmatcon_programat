<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('file')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('download_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('downloads_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('downloads_id')->references('id')->on('downloads')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('downloads');
        Schema::dropIfExists('download_translate');
    }
}
