<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaTranslate extends Model
{
    /**
     * @var string
     */
    protected $table = 'meta_translate';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'language_id', 'value', 'meta_id'
    ];
}