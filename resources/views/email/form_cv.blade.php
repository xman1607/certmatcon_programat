<table style="text-align:left;border:1px solid #c0c0c0;border-collapse:collapse;padding:5px">
    
    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Jobul la care sa atasat fisierul:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
            {{$request["job_title"]}}
        </td>
    </tr>

    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">CV-ul atasat:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
            <a href='{{$request["src_file"]}}'>{{$request["src_file"]}}</a>
        </td>
    </tr>

    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Adresa paginii de pe care sa trimis cv-ul:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px"><a href="{{$request['page_url']}}">{{$request["page_url"]}}</a></td>
    </tr>

</table>
<br>
- Aceasta scrisoare a fost trimisa de pe site-ul certmatcon (<a href="{{URL::to('/')}}">{{URL::to('/')}}</a>)