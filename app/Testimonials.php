<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class Testimonials extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'testimonials';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['name', 'text'];

    /**
     * @var
     */
    public $translationModel = TestimonialsTranslate::class;

    public function getTestimonials(){
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================

    public function scopeActive($query){
        $query->where('active', 1);
    }
}