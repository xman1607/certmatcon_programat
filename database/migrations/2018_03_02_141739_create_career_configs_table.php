<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bg_image')->nullable();
            $table->timestamps();
        });

        Schema::create('career_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('career_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->string('title_on_image')->nullable();

            $table->string('title_on_page')->nullable();
            $table->text('text_on_page')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('career_config_id')->references('id')->on('career_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_configs');
        Schema::dropIfExists('career_config_translate');
    }
}
