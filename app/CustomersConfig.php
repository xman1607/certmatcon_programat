<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class CustomersConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'customers_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
    								];


    /**
     * @var
     */
    public $translationModel = CustomersConfigTranslate::class;

}