<table style="text-align:left;border:1px solid #c0c0c0;border-collapse:collapse;padding:5px">
    
    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Numele utilizatorului:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
            {{$request["name"]}}
        </td>
    </tr>

    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Email-ul utilizatorului:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
            {{$request["email"]}}
        </td>
    </tr>

    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Telefonul utilizatorului:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
            {{$request["phone"]}}
        </td>
    </tr>

    <tr>
        <th style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">Mesajul utilizatorului:</th>
        <td style="vertical-align:middle;border:1px solid #c0c0c0;padding:5px">
        {{$request["message"]}}</td>
    </tr>

</table>
<br>
- Aceasta scrisoare a fost trimisa de pe site-ul certmatcon (<a href="{{URL::to('/')}}">{{URL::to('/')}}</a>)