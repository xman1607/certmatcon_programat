<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AboutUsAcredit;
use App\AboutUsConfig;
use App\Activities;
use App\ActivitiesConfig;
use App\Career;
use App\CareerConfig;
use App\ContactsConfig;
use App\CustomerCollaborations;
use App\Customers;
use App\CustomersConfig;
use App\DownloadConfig;
use App\Downloads;
use App\HomeBenefits;
use App\HomeConfig;
use App\HomeRemuneration;
use App\HomeSlider;
use App\News;
use App\NewsConfig;
use App\Personal;
use App\PersonLab;
use App\PersonalConfig;
use App\PersonLeader;
use App\Testimonials;
use App\TestimonialsConfig;

use Lang;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class MainController extends Controller
{
    
    public function index(
    	HomeConfig $homeConfigModel,
    	HomeSlider $homeSliderModel,
        AboutUsConfig $aboutUsConfigModel,
    	HomeBenefits $homeBenefitsModel,
    	HomeRemuneration $homeRemunerationModel,
    	News $newsModel
    ){

    	$home_config_data = $homeConfigModel->first();

    	$home_slider = $homeSliderModel->getItems();

        $home_about_us = $aboutUsConfigModel->first();

    	$home_benefits = $homeBenefitsModel->getBenefits();

    	$home_news = $newsModel->getNewsOnHomePage();

    	$acreditari = $homeRemunerationModel->getRemunerations();

    	foreach($home_news as $new){
    	    $year_month_day = explode("-", $new->public_date);
    	    $new->public_date = $year_month_day[2].".".$year_month_day[1].".".$year_month_day[0];
    	}

    	return view('pages.index', [
    		'home_config_data' => $home_config_data,
    		'home_slider' => $home_slider,
            'home_about_us' => $home_about_us,
    		'home_benefits' => $home_benefits,
    		'home_news' => $home_news,
    		'acreditari' => $acreditari
    	]);
    }

    public function about_us(
        AboutUsConfig $aboutUsConfigModel,
        AboutUsAcredit $aboutUsAcreditModel
    ){

        $about_us_config_data = $aboutUsConfigModel->first();

        $acredits = $aboutUsAcreditModel->getItems();

        return view('pages.about_us', [
            'about_us_config_data' => $about_us_config_data,
            'acredits' => $acredits
        ]);
    }

    public function activities(
        ActivitiesConfig $activitiesConfigModel,
        Activities $activitiesModel
    ){

        $activities_config_data = $activitiesConfigModel->first();

        $activities = $activitiesModel->getActivities();

        return view('pages.activities', [
            'activities_config_data' => $activities_config_data,
            'activities' => $activities
        ]);
    }

    public function customers(
        CustomersConfig $customersConfigModel,
        CustomerCollaborations $customerCollaborationsModel,
        Customers $customersModel
    ){

        $customers_config_data = $customersConfigModel->first();

        $collaborations = $customerCollaborationsModel->getItems();

        $customers = $customersModel->getItems();

        return view('pages.customers', [
            'customers_config_data' => $customers_config_data,
            'collaborations' => $collaborations,
            'customers' => $customers
        ]);
    }

    public function testimonials(
        TestimonialsConfig $testimonialsConfigModel,
        Testimonials $testimonialsModel
    ){

        $testimonials_config_data = $testimonialsConfigModel->first();

        $testimonials = $testimonialsModel->getTestimonials();

        return view('pages.testimonials', [
            'testimonials_config_data' => $testimonials_config_data,
            'testimonials' => $testimonials
        ]);
    }

    public function contacts(
        ContactsConfig $contactsConfigModel
    ){

        $contacts_config_data = $contactsConfigModel->first();

        return view('pages.contacts', [
            'contacts_config_data' => $contacts_config_data
        ]);
    }

    public function news(
        NewsConfig $newsConfigModel,
        News $newsModel
    ){

        $new_config_data = $newsConfigModel->first();
        $news = $newsModel->getNews();
        foreach($news as $new){
            $year_month_day = explode("-", $new->public_date);
            $nameMonth = $this->getNameMonths($year_month_day[1]);
            $new->public_date = $year_month_day[2]." ".$nameMonth." ".$year_month_day[0];
        }

        return view('pages.news', [
            'new_config_data' => $new_config_data,
            'news' => $news
        ]);
    }

    public function page_new(
        $slug_new,
        News $newsModel
    ){

        $new_data = $newsModel->getNewOnSlug($slug_new);
        if(is_null($new_data)){

            return abort(404);
        }else{
            $year_month_day = explode("-", $new_data->public_date);
            $new_data->public_date = $year_month_day[2].".".$year_month_day[1].".".$year_month_day[0];
            
            return view('pages.page_new', [
                'new_data' => $new_data
            ]);
        }
        
    }

    public function download(
        DownloadConfig $downloadConfigModel,
        Downloads $downloadsModel
    ){

        $download_config_data = $downloadConfigModel->first();

        $downloads = $downloadsModel->getDownloads();

        return view('pages.download', [
            'download_config_data' => $download_config_data,
            'downloads' => $downloads
        ]);
    }

    public function career(
        CareerConfig $careerConfigModel,
        Career $careerModel
    ){

        $career_config_data = $careerConfigModel->first();

        $careers = $careerModel->getCareers();

        return view('pages.career', [
            'career_config_data' => $career_config_data,
            'careers' => $careers
        ]);
    }

    public function personal(
        PersonalConfig $personalConfigModel,
        PersonLeader $personLeaderModel,
        Personal $personalModel,
        PersonLab $personLabModel
    ){

        $personal_config_data = $personalConfigModel->first();

        //Conducerea
        $person_leaders = $personLeaderModel->getLeaders();

        //Personal
        $personals = $personalModel->getPersonals();

        //Laborator Încercări
        $person_lab = $personLabModel->getPersonLabs();

        return view('pages.personal', [
            'personal_config_data' => $personal_config_data,
            'person_leaders' => $person_leaders,
            'personals' => $personals,
            'person_lab'=> $person_lab
        ]);
    }

    public function sendFormular(Request $request){
        $request = $request->all();

        //убираем символы маски
        $del_symbols_tel = ["(", ")", "-", " "];
        $request["phone"] =str_replace($del_symbols_tel, '', $request["phone"]);


        $validator_result = Validator::make($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|max:191',
            'phone' => 'required|digits_between:8,14',
            'message' => 'required',
        ]);

        if ($validator_result->fails()) {

            return response()->json([
                        'result_valid' => 'error',
                        'error' => $validator_result->errors(),
                        'request' => $request
            ]);

        } else {

            $name = $request['name'];
            $email = $request['email'];
            $phone = $request['phone'];

            /*$post = 'entry.1903967822='.$name.'&entry.971463663='.$phone.'&entry.1941619235='.$email; 

            $curl = curl_init();
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($curl, CURLOPT_URL, 'https://docs.google.com/forms/d/e/1FAIpQLSfjAWSKyEfUV2dBk1dkR99ycl-bDjXwnqlNOUc68hYX7HGCPg/formResponse');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_exec($curl);
            //htmlspecialchars(curl_exec($curl)); 
            curl_close($curl);*/

            $send = true;

            //$request["page_url"] = url()->previous();

            
            /*$send = Mail::send('email.form_contact', ['request' => $request], function ($m) use ($request) {
                $m->from($request["email"], 'Utilizator de pe site');

                $m->to('xmantest3@gmail.com', 'Delure')->subject('Formularul de pe site - pagina contacte');
            });*/


            if($send){

                return response()->json([
                    'result_valid' => 'succes',
                    'succes' => 'yessss'
                ]);

            }else{
                return response()->json([
                    'result_valid' => 'error',
                    //'message' => 'Eroare la transmiterea scrisorii pe email'
                ]);
            }
        }
    }

    public function sendFormularCustomers(Request $request){

        $request = $request->all();

        $validator_result = Validator::make($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|max:191',
            'question' => 'required',
        ]);

        if ($validator_result->fails()) {

            return response()->json([
                        'result_valid' => 'error',
                        'error' => $validator_result->errors(),
                        'request' => $request
            ]);

        } else {

            $name = $request['name'];
            $email = $request['email'];
            $question = $request['question'];

            $send = true;

            //$request["page_url"] = url()->previous();

            
            /*$send = Mail::send('email.form_customer', ['request' => $request], function ($m) use ($request) {
                $m->from($request["email"], 'Intrebare de la utilizator');

                $m->to('xmantest3@gmail.com', 'Delure')->subject('Intrebare de la utilizator');
            });*/


            if($send){

                return response()->json([
                    'result_valid' => 'succes',
                    'succes' => 'yessss'
                ]);

            }else{
                return response()->json([
                    'result_valid' => 'error',
                    //'message' => 'Eroare la transmiterea scrisorii pe email'
                ]);
            }
        }

    }

    public function send_cv(Request $request){

        //https://webformyself.com/laravel-zagruzka-fajlov/
        //https://ru.stackoverflow.com/questions/381936/%D0%9F%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%82%D1%8C-%D1%87%D0%B5%D1%80%D0%B5%D0%B7-ajax-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5-%D1%84%D0%BE%D1%80%D0%BC%D1%8B-file
        $request = $request->all();

        $validator_result = Validator::make($request, [
            'file_cv' => 'max:10240',
        ]);

        if ($validator_result->fails()) {

            return response()->json([
                        'result_valid' => 'error',
                        'error' => $validator_result->errors()
            ]);

        } else {

                $file_cv = Input::file('file_cv');

                if($file_cv){
                    
                    //$$file_cv->move(public_path() . '/upload/file_cv', time().'_'.$file_cv->getClientOriginalName());

                    $file_original_name = $file_cv->getClientOriginalName();

                    $file_path = $file_cv->getPathName();

                    $result_attach = $file_cv->move(public_path() . '/upload/file_cv', time().'_'.$file_original_name);



                    $src_file = 'http://'.$_SERVER["HTTP_HOST"].'/upload/file_cv/'.time().'_'.$file_original_name;
                    //$src_file = 'http://moneyhouse.md/formular_home_page/'.time().'_'.$file_original_name;
                    
                    $request["src_file"] = $src_file;

                    if($result_attach){

                        $request["page_url"] = url()->previous();

                        $send = Mail::send('email.form_cv', ['request' => $request], function ($m) use ($request) {
                            $m->from('office@certmatcon.md', 'CV de pe site');

                            $m->to('office@certmatcon.md', 'Certmatcon')->subject('Formularul de pe site');
                        });

                        if(!$send){

                            return response()->json([
                                'result_valid' => 'success',
                                'message' => 'Fisierul a fost trimis cu success',
                                'src_file_success' => $src_file
                            ]);
                        }else{
                            return response()->json([
                                'result_valid' => 'error',
                                'message' => 'Eroare la transmiterea scrisorii pe email',

                            ]);
                        }

                        
                    }else{
                        return response()->json([
                            'result_valid' => 'error',
                            'message' => 'Eroare la incarcarea fisierului pe server',
                            'src_file_error' => $src_file,
                            'result_attach' => $result_attach
                        ]);
                    }

                }else{
                    return response()->json([
                        'result_valid' => 'error',
                        'message' => 'Eroare la trimiterea fisierului pe server'
                    ]);
                }
            }
    }

    private function getNameMonths($number_month){

        $current_lang = Lang::slug();

        if($current_lang == 'ro'){

            switch($number_month){

                case '01':
                    $nameMonth = 'Ianuarie';
                    break;
                case '02':
                    $nameMonth = 'Februarie';
                    break;
                case '03':
                    $nameMonth = 'Martie';
                    break;
                case '04':
                    $nameMonth = 'Aprilie';
                    break;
                case '05':
                    $nameMonth = 'Mai';
                    break;
                case '06':
                    $nameMonth = 'Iunie';
                    break;
                case '07':
                    $nameMonth = 'Iulie';
                    break;
                case '08':
                    $nameMonth = 'August';
                    break;
                case '09':
                    $nameMonth = 'Septembrie';
                    break;
                case '10':
                    $nameMonth = 'Octombrie';
                    break;
                case '11':
                    $nameMonth = 'Noiembrie';
                    break;
                case '12':
                    $nameMonth = 'Decembrie';
                    break;
            }

        }else if($current_lang == 'ru'){

            switch($nameMonth){

                case '01':
                    $nameMonth = 'Январь';
                    break;
                case '02':
                    $nameMonth = 'Февраль';
                    break;
                case '03':
                    $nameMonth = 'Март';
                    break;
                case '04':
                    $nameMonth = 'Апрель';
                    break;
                case '05':
                    $nameMonth = 'Май';
                    break;
                case '06':
                    $nameMonth = 'Июнь';
                    break;
                case '07':
                    $nameMonth = 'Июль';
                    break;
                case '08':
                    $nameMonth = 'Август';
                    break;
                case '09':
                    $nameMonth = 'Сентябрь';
                    break;
                case '10':
                    $nameMonth = 'Октябрь';
                    break;
                case '11':
                    $nameMonth = 'Ноябрь';
                    break;
                case '12':
                    $nameMonth = 'Декабрь';
                    break;
            }

        }else{
            switch($nameMonth){

                case '01':
                    $nameMonth = 'January';
                    break;
                case '02':
                    $nameMonth = 'February';
                    break;
                case '03':
                    $nameMonth = 'March';
                    break;
                case '04':
                    $nameMonth = 'April';
                    break;
                case '05':
                    $nameMonth = 'May';
                    break;
                case '06':
                    $nameMonth = 'June';
                    break;
                case '07':
                    $nameMonth = 'July';
                    break;
                case '08':
                    $nameMonth = 'August';
                    break;
                case '09':
                    $nameMonth = 'September';
                    break;
                case '10':
                    $nameMonth = 'October';
                    break;
                case '11':
                    $nameMonth = 'November';
                    break;
                case '12':
                    $nameMonth = 'December';
                    break;
            }
        }

        return $nameMonth;
    }

}
