@extends('layout')

@section('title')
    <title>{{ $download_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $download_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/download.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    
    <section class="scroll_1">
    	<div class="container">
    		<div class="row">

    			<div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
    				<div class="scrl1_up_text">
    					<h2>{!! $download_config_data->title_on_page !!}</h2>
    				</div>
    			</div>

    		</div>

            <div class="row">
            @foreach($downloads as $download)
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="download_block">
                        <h2>{!! $download->title !!}</h2>
                        <a href="{{ $download->file }}">{!! $meta->getMeta('download') !!}</a>
                        <img src="{{ $download->image }}" alt="">
                    </div>
                </div>

                @if($loop->iteration % 3 == 0)
            </div>
            <div class="row">
                @endif

            @endforeach
            </div>

    		<div class="row">
    			<div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
    				<div class="scrl1_text1">
                        {!! $download_config_data->text1 !!}
    				</div>
    			</div>
    		</div>

    		<div class="row">
    			<div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12 no-padding">
    				<div class="scrl1_text2">
                        {!! $download_config_data->text2 !!}
    				</div>
    			</div>
    		</div>


    	</div>
    </section>

@endsection

@section('footer_js')
    
@endsection