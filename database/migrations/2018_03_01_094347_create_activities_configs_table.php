<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('activities_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('activities_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('title_on_page')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('activities_config_id')->references('id')->on('activities_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities_configs');
        Schema::dropIfExists('activities_config_translate');
    }
}
