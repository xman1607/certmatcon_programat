<!-- footer -->
<footer id="footer_id">

<div class="container">
	<div class="row padding_footer1">

		<div class="col-md-2 no-padding col-sm-3 col-xs-6">
			<div class="logo_footer">
				<img src="{{ asset('assets/img/logo_footer.png') }}" alt="">
			</div>
		</div>

		<div class="col-md-offset-8 col-md-2 col-sm-offset-6 col-sm-3 col-xs-6">
			<div class="sotial_link">
				<a href="mailto:{!! settings()->getOption('site::email') !!}" target="_blank">
					<img src="{{ asset('assets/img/mail.png') }}" alt="">
				</a>

<!-- 						<a href="#" target="_blank">
					<img src="/assets/img/skype.png" alt="">
				</a> -->

				<a href="{!! settings()->getOption('site::facebook') !!}" target="_blank">
					<img src="{{ asset('assets/img/facebook.png') }}" alt="">
				</a>
			</div>
		</div>

		<hr>

	</div>

			<div class="row padding_footer2">

				<div class="col-md-offset-1 col-md-2 col-sm-6 col-xs-12 margin_footer">
					<div class="first_column">
						
						<a href="{{ route('news') }}">{!! $meta->getMeta('footer_news') !!}</a>
						<a href="#">{!! $meta->getMeta('footer_partners') !!}</a>
						<a href="{{ route('testimonials') }}">{!! $meta->getMeta('footer_testimonials') !!}</a>
						<a href="{{ route('customers') }}">{!! $meta->getMeta('footer_customers') !!}</a>
						<a href="{{ route('download') }}">{!! $meta->getMeta('footer_download') !!}</a>

					</div>
				</div>

				<div class="col-md-2 col-sm-6 col-xs-12 margin_footer">

					<div class="column_zagolovok">

						<a href="{{ route('about_us') }}" class="zagolovok_footer">{!! $meta->getMeta('footer_about_us') !!}</a>

						<div class="sub_zagolovok">

							<a href="#">{!! $meta->getMeta('footer_acreditations') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_general_prezentation') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_short_history') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_leadership') !!}</a>
							<a href="{{ route('personal') }}">{!! $meta->getMeta('footer_personal') !!}</a>

						</div>

					</div>

				</div>

				<div class="col-md-2 col-sm-6 col-xs-12 margin_footer">
					<div class="last_column">
						
						<a href="#">{!! $meta->getMeta('footer_organigrama') !!}</a>
						<a href="#">{!! $meta->getMeta('footer_mission_concept_strategy') !!}</a>
						<a href="{{ route('news') }}">{!! $meta->getMeta('footer_news') !!}</a>
						<a href="#">{!! $meta->getMeta('footer_photo_gallery') !!}</a>

					</div>
				</div>

				<div class="col-md-2 col-sm-6 col-xs-12 margin_footer">
					<div class="column_zagolovok">
						
						<a href="{{ route('activities') }}" class="zagolovok_footer">{!! $meta->getMeta('footer_activity') !!}</a>

						<div class="sub_zagolovok">

							<a href="#">{!! $meta->getMeta('footer_materials_and_products') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_laborator_tries') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_furniture') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_tries_radiologie') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_certificates') !!}</a>
							<a href="#">{!! $meta->getMeta('footer_ask_offer') !!}</a>
							<a href="#"></a>

						</div>

					</div>
				</div>

				<div class="col-md-offset-1 col-md-2 col-sm-6 col-xs-12 margin_footer">

					<div class="column_zagolovok">
						
						<a href="{{ route('contacts') }}" class="zagolovok_footer">{!! $meta->getMeta('footer_contacts') !!}</a>

						<div class="sub_zagolovok">
							<ul>

								<li>
									@php $del_symbols_tel = ["(", ")", " "] @endphp
									<a href="tel:{{ str_replace($del_symbols_tel, '', settings()->getOption('site::phone'))}}">{!! settings()->getOption('site::phone') !!}</a>
								</li>

								<li>
									{!! settings()->getOption('site::address') !!}
								</li>

							</ul>

						</div>

					</div>

				</div>


			</div>

			<div class="row">
				
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="copirite">
						<h4>
							&copy; {{ date('Y') }} {!! $meta->getMeta('footer_all_rights_reserved') !!} 
						</h4>
					</div>
				</div>

				<div class="col-md-offset-7 col-md-2 col-sm-6 col-xs-12">

					<div class="marsala">
						<a href="http://marsala.md" target="_blank">Developed by <img src="{{ asset('assets/img/marsala.png') }}" alt=""></a>
					</div>

				</div>

			</div>

		</div>
	</footer>