<div id="menu_id">
	<!-- menu mobile -->

	<div class="mobile_menu">
		
		<div class="logo">
			<a href="{{ route('index') }}">
				<img src="{{ asset('assets/img/logo_menu.png') }}" alt="">
			</a>
		</div>

		<div class="number">
			
			@php $del_symbols_tel = ["(", ")", " "] @endphp
			<a href="tel:{{ str_replace($del_symbols_tel, '', settings()->getOption('site::phone_fix'))}}">{!! settings()->getOption('site::phone_fix') !!}</a>

			<div class="burg_menu">
				<span></span>
				<span></span>
				<span></span>
			</div>

		</div>

	</div>

	<!-- menu -->

	<div class="menu">

		<hr>

		<div class="line_1">
			<div class="row">

				<div class="col-md-2">
					<div class="logo">
						<a href="{{ route('index') }}">
							<img src="{{ asset('assets/img/logo_menu.png') }}" alt="">
						</a>
					</div>
				</div>

				<div class="col-md-offset-6 col-md-4">

					<div class="number_and_language">
						@php $del_symbols_tel = ["(", ")", " "] @endphp
						<a href="tel:{{ str_replace($del_symbols_tel, '', settings()->getOption('site::phone'))}}"><img src="{{ asset('assets/img/tel.png') }}" alt="">
							{!! settings()->getOption('site::phone') !!}
						</a>
						<div class="language_block">
							@foreach($languages['all'] as $lang)
							        
								@if($languages['current']->slug == $lang->slug)
									<a class="active" href="/{{ $lang->slug }}">{{$lang->title}}</a>
								@else
									<a href="/{{ $lang->slug }}">{{$lang->title}}</a>
								@endif
							@endforeach
						</div>
					</div>

				</div>

			</div>
		</div>

		<div class="line_2">

			<div class="col-md-offset-2 col-md-10">

				<div class="line_2_menu">

					<ul>
						<li>
							<a href="{{ route('index') }}">{!! $meta->getMeta('home') !!}</a>
						</li>

						<li>
							<a href="{{ route('about_us') }}">{!! $meta->getMeta('about_us') !!}</a>
						</li>

						<li>
							<a href="{{ route('activities') }}">{!! $meta->getMeta('activity') !!}</a>
						</li>

						<li>
							<a href="{{ route('customers') }}">{!! $meta->getMeta('customers') !!}</a>
						</li>

						<li>
							<a href="{{ route('testimonials') }}">{!! $meta->getMeta('testimonials') !!}</a>
						</li>

						<li>
							<a href="{{ route('contacts') }}">{!! $meta->getMeta('contacts') !!}</a>
						</li>

						<li>
							<a href="{{ route('news') }}">{!! $meta->getMeta('news') !!}</a>
						</li>

						<li>
							<a href="{{ route('download') }}">{!! $meta->getMeta('download') !!}</a>
						</li>
					</ul>

				</div>

			</div>

		</div>

	</div>

</div>