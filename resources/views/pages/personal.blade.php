@extends('layout')

@section('title')
    <title>{{ $personal_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $personal_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/echypa.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    

    <section class="scroll_1" style="background: url({{str_replace('\\','/',$personal_config_data->bg_image)}}) center no-repeat / cover;">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="scrl1_content zindex">
                        <h2>{!! $personal_config_data->title_on_image !!}</h2>
                        <h4>{!! $personal_config_data->subtitle_on_image !!}</h4>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="scroll_2">
        
        <div class="container">
            <div class="row">

                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12 no-padding">
                    <div class="scrl2_up_text">
                        <h3>{!! $meta->getMeta('leadership') !!}</h3>
                    </div>
                </div>
                
                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 no-padding">

                    <div class="scrl_cards_parent cards_left">
                        
                        @php $del_symbols_tel = ["(", ")", "-", ".", " "] @endphp

                        @foreach($person_leaders as $leader)
                            <div class="scrl_cards">

                                <div class="scrl_cards_img" style="background: url({{str_replace('\\','/',$leader->avatar)}}) center no-repeat / cover;"></div>

                                <h3>{!! $leader->name !!}</h3>
                                <h4>{!! $leader->function !!}</h4>

                                <div class="scrl_cards_content">
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $leader->fax)}}">
                                        <span>{!! $meta->getMeta('fax') !!}</span> 
                                        {!! $leader->fax !!}
                                    </a>
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $leader->phone)}}">
                                        <span>{!! $meta->getMeta('phone') !!}</span> 
                                        {!! $leader->phone !!}
                                    </a>
                                    <a href="mailto:{{$leader->email}}">
                                        <span>{!! $meta->getMeta('email') !!}</span> 
                                        {!! $leader->email !!}
                                    </a>
                                </div>

                            </div>
                        @endforeach

                    </div>

                </div>

            </div>
        </div>

    </section>

    <section class="scroll_3">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12 no-padding">
                    <div class="scrl3_up_text">
                        <h2>{!! $meta->getMeta('personal') !!}</h2>
                        <h3>{!! $meta->getMeta('subtitle_personal') !!}</h3>
                        <h4>{!! $meta->getMeta('personal_description') !!}</h4>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 no-padding">
                    <div class="scrl_cards_parent">

                        @foreach($personals as $person)
                            <div class="scrl_cards">

                                <div class="scrl_cards_img" style="background: url({{str_replace('\\','/',$person->avatar)}}) center no-repeat / cover;"></div>

                                <h3>{!! $person->name !!}</h3>
                                <h4>{!! $person->function !!}</h4>

                                <div class="scrl_cards_content">
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $person->fax)}}">
                                        <span>{!! $meta->getMeta('fax') !!}</span> 
                                        {!! $person->fax !!}
                                    </a>
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $person->phone)}}">
                                        <span>{!! $meta->getMeta('phone') !!}</span> 
                                        {!! $person->phone !!}
                                    </a>
                                    <a href="mailto:{{$person->email}}">
                                        <span>{!! $meta->getMeta('email') !!}</span> 
                                        {!! $person->email !!}
                                    </a>
                                </div>

                            </div>
                        @endforeach

                    </div>
                </div>

                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12 no-padding">
                    <div class="scrl3_center_text">
                        <h3>{!! $meta->getMeta('laborator_tries') !!}</h3>
                        <h4>{!! $meta->getMeta('laborator_tries_description') !!}</h4>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 no-padding">
                    <div class="scrl_cards_parent">

                        @foreach($person_lab as $person)
                            <div class="scrl_cards">

                                <div class="scrl_cards_img" style="background: url({{str_replace('\\','/',$person->avatar)}}) center no-repeat / cover;"></div>

                                <h3>{!! $person->name !!}</h3>
                                <h4>{!! $person->function !!}</h4>

                                <div class="scrl_cards_content">
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $person->fax)}}">
                                        <span>{!! $meta->getMeta('fax') !!}</span> 
                                        {!! $person->fax !!}
                                    </a>
                                    <a href="tel:{{ str_replace($del_symbols_tel, '', $person->phone)}}">
                                        <span>{!! $meta->getMeta('phone') !!}</span> 
                                        {!! $person->phone !!}
                                    </a>
                                    <a href="mailto:{{$person->email}}">
                                        <span>{!! $meta->getMeta('email') !!}</span> 
                                        {!! $person->email !!}
                                    </a>
                                </div>

                            </div>
                        @endforeach

                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                    <div class="scrl2_img">
                        <a class="fancybox" data-fancybox href="{{ $personal_config_data->bottom_image }}">
                            <img src="{{ $personal_config_data->bottom_image }}" alt="">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('footer_js')
    
@endsection