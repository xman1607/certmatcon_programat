<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class CustomerCollaborations extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'customer_collaborations';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'title'
	];


    /**
     * @var
     */
    public $translationModel = CustomerCollaborationsTranslate::class;

    public function getItems(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}