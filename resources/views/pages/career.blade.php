@extends('layout')

@section('title')
    <title>{{ $career_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $career_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/cariere.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <section class="scroll_1" style="background: url({{str_replace('\\','/',$career_config_data->bg_image)}}) center no-repeat / cover;">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-3 col-md-6">
                    <div class="scrl1_content">
                        <h3>{!! $career_config_data->title_on_image !!}</h3>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="scroll_2">

        <div class="container">
            <div class="row">

                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12 no-padding">

                    <div class="scrl2_up_text">
                        <h2>{!! $career_config_data->title_on_page !!}</h2>
                        <h4>{!! $career_config_data->text_on_page !!}</h4>
                    </div>

                </div>

                <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
                    
                    @if(count($careers) > 0)
                        
                        @foreach($careers as $carrer)
                            <div class="scrl2_collapse">
                                <h3  data-toggle="collapse" data-target="#block{{ $loop->iteration }}">
                                    {!! $carrer->title !!}
                                </h3>
                                <h4 data-toggle="collapse" data-target="#block{{ $loop->iteration }}">
                                    {!! $carrer->location !!}
                                </h4>
                                <img src="/assets/img/cariere/arrow.png" alt="" class="arrow_collapse" data-toggle="collapse" data-target="#block{{ $loop->iteration }}">

                                <div id="block{{ $loop->iteration }}" class="collapse block_down">

                                    {!! $carrer->text !!}

                                    <form id="form_post_{{$loop->iteration}}">
                                        <p>{!! $meta->getMeta('maxim_size') !!}</p>
                                        <label for="#file">
                                            <input type="file" name="file_{{$loop->iteration}}" class="input" required="required">
                                            <span class="input_text">
                                                {!! $meta->getMeta('attempt_the_file') !!}
                                            </span>
                                            <img src="/assets/img/cariere/file.png" alt="">
                                        </label>
                                        
                                        <input type="hidden" name="job_title" value="{{ $carrer->title }}">

                                        <button>{!! $meta->getMeta('send') !!}</button>
                                    </form>
                                </div>
                            </div>
                        @endforeach

                    @else
                        <div class="scrl2_collapse">
                            <h3 class="text-center">{!! $meta->getMeta('no_post') !!}</h3>
                        </div>
                    @endif

                </div>

            </div>
        </div>

    </section>

@endsection

@section('footer_js')
    
    <script>

        var file;
        $(".input").change(function(event) {
            if ($(this).val().lastIndexOf('\\')) {
                var i = $(this).val().lastIndexOf('\\') + 1;
            } else {
                var i = $(this).val().lastIndexOf('/') + 1;
            }
            var fileName = $(this).val().slice(i);
            $(this).siblings('.input_text').html(fileName);

            var files = event.target.files;
            file = files[0];

        });


        $('form').on('submit', function(event){
            event.preventDefault();

            var this_form = $(this);

            var formData = new FormData();

            formData.append('file_cv', file);

            var request = new XMLHttpRequest();

            request.open('POST', '{{route('send_cv')}}');

            var token = $('meta[name="csrf-token"]').attr('content');
            request.setRequestHeader('X-CSRF-TOKEN', token);

            request.send(formData);

            request.addEventListener('load', function(){

                var response = JSON.parse(request.responseText);
                //console.log(response.result_valid);

                if(response.result_valid == 'success'){
                    
                    $('.input_text').removeClass('text-success');
                    $('.input_text').addClass('text-success');
                    $('.input_text').html("{!! $meta->getMeta('success_send_email') !!}");
                    this_form[0].reset();

                }else{
                    $('.input_text').removeClass('text-danger');
                    $('.input_text').addClass('text-danger');
                    $('.input_text').html("{!! $meta->getMeta('error_send_email') !!}");
                    this_form[0].reset();
                }
            });
        });

    </script>

@endsection