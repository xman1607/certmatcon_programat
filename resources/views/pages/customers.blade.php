@extends('layout')

@section('title')
    <title>{{ $customers_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $customers_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/partenero.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    <section class="scroll_1">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="scrl1_text">
                        <h2>{!! $meta->getMeta('collaborations') !!}</h2>
                        <h3>{!! $meta->getMeta('collaborations_description') !!}</h3>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                    <div class="scrl_logo_slider">
                        
                        <div class="slide">
                        @foreach($collaborations as $collaboration)
                            
                            <div class="logo_block">
                                <div class="img_block">
                                    <img src="{{ asset( $collaboration->logo ) }}" alt="{{ $collaboration->title }}" width="80">
                                </div>
                                <div class="text_block">
                                    <h3>{!! $collaboration->title !!}</h3>
                                </div>
                            </div>

                            @if($loop->iteration % 3 == 0)
                        </div>
                        <div class="slide">
                            @endif

                        @endforeach
                        </div>

                    </div>
                </div>

                <div class="col-md-offset-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="scrl1_text scrl1_text2">
                        <h2>{!! $meta->getMeta('customers') !!}</h2>
                        <h3>{!! $meta->getMeta('customers_description') !!}</h3>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                    <div class="scrl_logo_slider">

                        <div class="slide">
                        @foreach($customers as $customer)
                            
                            <div class="logo_block">
                                <div class="img_block">
                                    <img src="{{ asset( $customer->avatar ) }}" alt="{{ $customer->name }}" width="80">
                                </div>
                                <div class="text_block">
                                    <h3>{!! $customer->name !!}</h3>
                                </div>
                            </div>

                            @if($loop->iteration % 3 == 0)
                        </div>
                        <div class="slide">
                            @endif

                        @endforeach
                        </div>

                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form id="customer_form">
                        
                        <div class="form_block">
                            
                            <h4>{!! $meta->getMeta('become_our_partner') !!}</h4>
                            <input type="text" name="name" placeholder="{!! $meta->getMeta('customer_name') !!}" required minlength="3" maxlength="191">
                            <input type="text" name="email" class="email" placeholder="{!! $meta->getMeta('customer_email') !!}" required minlength="3" maxlength="191" >

                            <input type="text" name="question" placeholder="{!! $meta->getMeta('customer_question') !!}" required minlength="3" maxlength="191" >
                            
                            <p class="note_message text-center"></p>

                            <button class="send_button">{!! $meta->getMeta('customer_send') !!}</button>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('footer_js')
    
    <script type="text/javascript">
        
        $(document).ready(function(){

            var email_validation;
            var email_validation_popup;
            //https://ruseller.com/lessons.php?rub=32&id=152
            var pattern_email = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

            var formular_note_message = $('form#customer_form .note_message');

            // Validarea cimpului email
            $('form#customer_form .email').blur(function(){

                var email_val = $(this).val();

                if(email_val.length > 0){

                    email_validation = pattern_email.test(email_val);

                    if(!email_validation){
                        
                        $(this).css('border-bottom-color','#ec3938');

                       formular_note_message.html("{!! $meta->getMeta('incorect_format_email') !!}");
                       formular_note_message.addClass('alert-danger');

                       $('.send_button').css('background', '#ccc');
                       $('.send_button').attr('disabled', true);

                    }else{
                        $(this).css({'border-bottom-color':'#e1e1e1'});

                        formular_note_message.empty();
                        formular_note_message.removeClass('alert-danger');

                        $('.send_button').css('background', '#d0422c');
                        $('.send_button').attr('disabled', false);
                    }

                }else{
                    email_validation = false;

                     $(this).css({'border-bottom-color':'#ec3938'});

                    formular_note_message.html("{!! $meta->getMeta('complet_required_fields') !!}");
                    formular_note_message.addClass('alert-danger');

                    $('.send_button').css('background', '#ccc');
                    $('.send_button').attr('disabled', true);
                }
            });

            $('form#customer_form .send_button').click(function(event) {

                event.preventDefault();
                var form = $(this).parent('form');

                var form_data_serialize = $(this).parents('form').serialize();

                //Проверяем если заполнены все поля
                var required_filds = $(this).parents('form').find("[required]");
                var empty_filds = [];

                var send_button = $(this);

                $(required_filds).each(function(index){
                    if ( $(this).val() == '' ){

                        empty_filds[index] = $(this);

                        $(this).css('border-bottom-color', '#ec3938');
                        $(this).focus();

                        formular_note_message.html("{{ $meta->getMeta('complet_required_fields') }}");
                        
                        formular_note_message.addClass('alert-danger');

                        send_button.css('background', '#ccc');
                        send_button.attr('disable', true);

                        setTimeout(function() {

                            formular_note_message.empty();
                            formular_note_message.removeClass('alert-danger');

                            send_button.css('background', '#d0422c');
                            send_button.attr('disable', false);
                        }, 3000);

                    }else{
                        $(this).css({'border-bottom-color':'#e1e1e1'});
                    }
                });

                if(empty_filds.length == 0){

                    //daca emailul e valid
                    if(email_validation){

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '{{route('sendFormularCustomers')}}',
                            method: 'POST',
                            dataType: 'json',
                            data: form_data_serialize,

                            beforeSend: function(){
                            
                                formular_note_message.html("{!! $meta->getMeta('send_message') !!}");
                                formular_note_message.removeClass('alert-success');
                                formular_note_message.removeClass('alert-danger');
                                formular_note_message.addClass('alert-info');
                            },

                            success: function(data) {

                                if (data.result_valid == 'succes') {

                                    formular_note_message.html("{{ $meta->getMeta('message_send_success') }}");

                                    formular_note_message.removeClass('alert-info');
                                    formular_note_message.addClass('alert-success');

                                    setTimeout(function() {
                                        formular_note_message.empty();
                                        formular_note_message.removeClass('alert-success');
                                    }, 3000);

                                    $('form#customer_form').trigger('reset');


                                } else {
                                    
                                    formular_note_message.html("{{ $meta->getMeta('message_send_error') }}");
                                    formular_note_message.removeClass('alert-info');
                                    formular_note_message.addClass('alert-danger');

                                    setTimeout(function() {
                                        formular_note_message.empty();
                                        formular_note_message.removeClass('alert-danger');
                                    }, 3000);

                                }
                            },
                            async: false
                        });
                    }
                }

            });

        });

    </script>

@endsection