<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonLeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_leaders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar')->nullable();
            $table->string('fax')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('position')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('person_leader_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('person_leader_id');
            $table->unsignedInteger('language_id');

            $table->string('name')->nullable();
            $table->string('function')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('person_leader_id')->references('id')->on('person_leaders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_leaders');
        Schema::dropIfExists('person_leader_translate');
    }
}
