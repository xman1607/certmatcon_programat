@extends('layout')

@section('title')
    <title>{{ $new_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $new_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/noutati.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <div class="scroll_1">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="scrl1_up_text">
                        <h3>{!! $new_config_data->title_on_page !!}</h3>
                    </div>
                </div>

                @if(count($news) > 0)
                    @foreach($news as $new)
                        <div class="col-md-offset-2 col-md-10 col-sm-offset-1 col-sm-11 col-xs-12">

                            <div class="news_block">

                                <div class="news_block_img" style="background: url({{str_replace('\\','/',$new->image_preview)}}) center no-repeat / cover;"></div>

                                <div class="new_block_content">

                                    <h4>{{ $new->public_date }}</h4>
                                    <hr>

                                    <h3>{!! $new->title !!}</h3>
                                    <h5>{!! $new->excerpt !!}</h5>

                                    <a href="{{ route('page_new', ['slug_new' => $new->slug] ) }}">{!! $meta->getMeta('read_more') !!}</a>

                                </div>

                            </div>

                        </div>
                    @endforeach

                    <!-- переключение страниц -->
                    @if ($news->lastPage() > 1)
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="str_block">
                                <div class="slick_str">
                                <!-- <div class="{{ ($news->currentPage() == 1) ? 'pages_inner disabled' : 'pages_inner' }}">
                                    <a href="{{ $news->url(1) }}">&lt;</a>
                                </div> -->
                                @for ($i = 1; $i <= $news->lastPage(); $i++)

                                    <div class="{{ ($news->currentPage() == $i) ? 'slide button_str active' : 'slide button_str' }}">
                                        <a href="{{ $news->url($i) }}">{{ $i }}</a>
                                    </div>
                                @endfor
                                
                                <!-- <div class="pages_inner {{ ($news->currentPage() == $news->lastPage()) ? ' disabled' : '' }}">
                                    <a href="{{ $news->url($news->currentPage()+1) }}" >&gt;</a>
                                </div> -->
                                </div>
                            </div>
                        </div>
                    @endif

                @else
                    <h3 style="text-align: center; margin-bottom: 30px;">
                        {!! $meta->getMeta('no_news') !!}
                    </h3>
                @endif

            </div>
        </div>
    </div>

@endsection

@section('footer_js')

@endsection