<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class Downloads extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'downloads';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title'];

    /**
     * @var
     */
    public $translationModel = DownloadsTranslate::class;

    public function getDownloads(){
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================

    public function scopeActive($query){
        $query->where('active', 1);
    }
}