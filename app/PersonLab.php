<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class PersonLab extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'person_labs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'name', 
    	'function'
    ];

    /**
     * @var
     */
    public $translationModel = PersonLabTranslate::class;

    public function getPersonLabs(){
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================

    public function scopeActive($query){
        $query->where('active', 1);
    }
}