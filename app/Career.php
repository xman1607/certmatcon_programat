<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class Career extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'careers';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'title',
    	'text',
    	'location'
	];


    /**
     * @var
     */
    public $translationModel = CareerTranslate::class;

    public function getCareers(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}