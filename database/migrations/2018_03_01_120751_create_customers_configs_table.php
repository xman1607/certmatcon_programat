<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('customers_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('customers_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('customers_config_id')->references('id')->on('customers_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_configs');
        Schema::dropIfExists('customers_config_translate');
    }
}
