<?php

use App\News;

return [
    'title'  => 'Noutati',

    'description' => 'Noutati',

    'model'  => News::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        //'position',

        'title',

        'image_preview' => column_element('', false, '<img width="100" src="(:image_preview)" />'),

        'slug',

        'public_date',

       'on_home_page' => column_element('Pe pagina principala', false, function($row)
       {
           return output_boolean($row, 'on_home_page');
       }),

        'active' => column_element('Activa', false, function($row)
        {
            return output_boolean($row);
        }),

    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query->orderBy('public_date', 'desc');
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'slug' => form_text('Adresa la pagina noutatii. Puteti introduce doar litere din alfabetul latin, cifre si cratima. Litere din alfabetul rus sunt interzise.',[ 'required' => 'required']),

        'image_preview' => [
            'label' => 'Imaginea de previualizare',
            'type'=> 'image',
            'location'=> 'upload/news'
        ],

        'title' => form_text('Titlul Noutatii') + translatable(),

        'meta_description' => form_textarea('Descrierea noutatii ( Pentru SEO )') + translatable(),

        'excerpt' => form_textarea('Descrierea pe pagina Noutati') + translatable(),

        'text' => form_ckeditor('Textul noutatii') + translatable(),

        'public_date' => form_date('Data publicarii', ['required' => 'required']),

        //'position'    => form_number($label = 'Ordinea. Numar intreg. Cu cit e mai mare, cu atit e mai la dreapta.', ['min' => 1, 'max' => 100000, 'required' => 'required']),

       'on_home_page' => filter_select('De afisat pe pagina principala', [
           0 => 'Nu',
           1 => 'Da'

       ]),

        'active' => filter_select('Activa', [
            1 => 'Da',
            0 => 'Nu'
        ]),

    ]
];