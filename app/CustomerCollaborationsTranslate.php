<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class CustomerCollaborationsTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'customer_collaboration_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							
						];

    /**
     * @var bool
     */
    public $timestamps = false;

}