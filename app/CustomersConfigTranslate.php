<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class CustomersConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'customers_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
						];

    /**
     * @var bool
     */
    public $timestamps = false;

}