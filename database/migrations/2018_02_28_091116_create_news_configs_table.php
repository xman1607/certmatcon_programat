<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('news_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('news_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('title_on_page')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('news_config_id')->references('id')->on('news_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_configs');
        Schema::dropIfExists('news_config_translate');
    }
}
