<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$autoIncrement = autoIncrement();
$factory->define(App\CustomerCollaborations::class, function (Faker\Generator $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'title' => $faker->name,
        'position' => $autoIncrement->current(),
        'active' => 1
    ];
});

$factory->define(App\Customers::class, function (Faker\Generator $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'name' => $faker->name,
        'position' => $autoIncrement->current(),
        'active' => 1
    ];
});

$factory->define(App\PersonLeader::class, function (Faker\Generator $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'name' => $faker->name,
        'function' => str_random(10),
        'fax' => $faker->phoneNumber,
        'phone' => $faker->tollFreePhoneNumber,
        'email' => $faker->email,
        'position' => $autoIncrement->current(),
        'active' => 1
    ];
});

$factory->define(App\Personal::class, function (Faker\Generator $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'name' => $faker->name,
        'function' => str_random(10),
        'fax' => $faker->phoneNumber,
        'phone' => $faker->tollFreePhoneNumber,
        'email' => $faker->email,
        'position' => $autoIncrement->current(),
        'active' => 1
    ];
});

$factory->define(App\PersonLab::class, function (Faker\Generator $faker) use ($autoIncrement) {
    $autoIncrement->next();
    return [
        'name' => $faker->name,
        'function' => str_random(10),
        'fax' => $faker->phoneNumber,
        'phone' => $faker->tollFreePhoneNumber,
        'email' => $faker->email,
        'position' => $autoIncrement->current(),
        'active' => 1
    ];
});

function autoIncrement()
{
    for ($i = 0; $i < 32; $i++) {
        yield $i;
    }
}
