<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class HomeBenefits extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'home_benefits';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'title',
    	'text'
    ];


    /**
     * @var
     */
    public $translationModel = HomeBenefitsTranslate::class;

    public function getBenefits(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}