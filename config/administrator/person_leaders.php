<?php

use App\PersonLeader;

return [
    'title'  => 'Conducerea',

    'description' => 'Conducerea',

    'model'  => PersonLeader::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        'position',

        'avatar' => column_element('', false, '<img width="100" src="(:avatar)" />'),

        'name',

        'function',

        'fax',

        'phone',

        'email',

        'active' => column_element('Activ', false, function($row)
        {
            return output_boolean($row);
        }),
    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query->orderBy('position');
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'avatar'       => [
            'label' => 'Imaginea',
            'type'=> 'image',
            'location'=> 'upload/personal/avatars'
        ],

        'name' => form_text('Nume Prenume') + translatable(),

        'function' => form_text('Functia') + translatable(),

        'fax' => form_text('Fax'),

        'phone' => form_text('Telefon'),

        'email' => form_text('E-mail'),


        'position'    => form_number($label = 'Ordinea. Numar intreg. Cu cit e mai mare, cu atit e mai la dreapta.', ['min' => 1, 'max' => 100000, 'required' => 'required']),

        'active' => filter_select('Activ', [
            1 => 'Da',
            0 => 'Nu'
        ]),

    ]
];