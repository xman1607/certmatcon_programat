<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('download_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('download_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->string('title_on_page')->nullable();

            $table->text('text1')->nullable();

            $table->text('text2')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('download_config_id')->references('id')->on('download_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download_configs');
        Schema::dropIfExists('download_config_translate');
    }
}
