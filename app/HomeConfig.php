<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class HomeConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'home_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
                                    'title_on_slider',
                                    'subtitle_on_slider'
    							];


    /**
     * @var
     */
    public $translationModel = HomeConfigTranslate::class;

}