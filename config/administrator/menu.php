<?php

/**
 * The menu structure of the site. For models, you should either supply the name of a model config file or an array of names of model config
 * files. The same applies to settings config files, except you must prepend 'settings.' to the settings config file name. You can also add
 * custom pages by prepending a view path with 'page.'. By providing an array of names, you can group certain models or settings pages
 * together. Each name needs to either have a config file in your model config path, settings config path with the same name, or a path to a
 * fully-qualified Laravel view. So 'users' would require a 'users.php' file in your model config path, 'settings.site' would require a
 * 'site.php' file in your settings config path, and 'page.foo.test' would require a 'test.php' or 'test.blade.php' file in a 'foo' directory
 * inside your view directory.
 *
 * @type array
 *
 *    array(
 *        'E-Commerce' => array('collections', 'products', 'product_images', 'orders'),
 *        'homepage_sliders',
 *        'users',
 *        'roles',
 *        'colors',
 *        'Settings' => array('settings.site', 'settings.ecommerce', 'settings.social'),
 *        'Analytics' => array('E-Commerce' => 'page.ecommerce.analytics'),
 *    )
 */
return [
    'dashboard' => [
        'icon' => 'fa-dashboard',
    ],

    /*'Roles' => [
        'page_header' => 'not working',
        'pages' => [
            'admins' => [
                'icon' => 'fa fa-user'
            ],
            'members' => [
                'icon' => 'fa fa-users'
            ]
        ]
    ],
    'Content' => [
        'page_header' => 'main content site',
        'pages' => [
            'pages' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ]*/

    'meta_header' => [
        'icon' => 'fa fa-file-text-o',
    ],

    'meta_footer' => [
        'icon' => 'fa fa-file-text-o',
    ],

    'meta_404' => [
        'icon' => 'fa fa-file-text-o',
    ],

    'Acasa' => [
        //'page_header' => 'main content site',
        'pages' => [
            'meta_home' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'home_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/home_config/1/edit?hide_return_create=1'
            ],

            'home_sliders' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'home_benefits' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'home_remunerations' => [
                'icon' => 'fa fa-file-text-o'
            ],
        ]
    ],

    'Despre noi' => [
        //'page_header' => '',
        'pages' => [

            'meta_about_us' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'about_us_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/about_us_config/1/edit?hide_return_create=1'
            ],
            
            'about_us_acredit' => [
                'icon' => 'fa fa-file-text-o'
            ],
        ]
    ],

    'Activitati' => [
        //'page_header' => '',
        'pages' => [

            'meta_activities' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'activities_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/activities_config/1/edit?hide_return_create=1'
            ],
            
            'activities' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ],

    'Clienti' => [
        //'page_header' => '',
        'pages' => [

            'meta_customers' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'customers_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/customers_config/1/edit?hide_return_create=1'
            ],

            'customers_collaborations' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'customers' => [
                'icon' => 'fa fa-file-text-o'
            ],
        ]
    ],

    'Marturii' => [
        //'page_header' => '',
        'pages' => [

            'meta_testimonials' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'testimonials_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/testimonials_config/1/edit?hide_return_create=1'
            ],

            'testimonials' => [
                'icon' => 'fa fa-file-text-o'
            ],
        ]
    ],

    'Contacte' => [
        //'page_header' => '',
        'pages' => [

            'meta_contacts' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'contacts_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/contacts_config/1/edit?hide_return_create=1'
            ],
        ]
    ],

    'Noutati' => [
        //'page_header' => '',
        'pages' => [

            'meta_news' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'news_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/news_config/1/edit?hide_return_create=1'
            ],
            
            'news' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ],

    'Download' => [
        //'page_header' => '',
        'pages' => [

            'meta_download' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'download_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/download_config/1/edit?hide_return_create=1'
            ],

            'downloads' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ],

    'Cariere' => [
        //'page_header' => '',
        'pages' => [

            'meta_career' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'career_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/career_config/1/edit?hide_return_create=1'
            ],

            'careers' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ],

    'Personal' => [
        //'page_header' => '',
        'pages' => [

            'meta_personal' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'personal_config' => [
                'icon' => 'fa fa-file-text-o',
                'link' => '/admin/personal_config/1/edit?hide_return_create=1'
            ],

            'person_leaders' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'personals' => [
                'icon' => 'fa fa-file-text-o'
            ],

            'person_labs' => [
                'icon' => 'fa fa-file-text-o'
            ]
        ]
    ],
];
