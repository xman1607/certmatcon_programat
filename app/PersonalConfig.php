<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class PersonalConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'personal_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
    								'title_on_image',
    								'subtitle_on_image',
    								'title_on_page'
    								];


    /**
     * @var
     */
    public $translationModel = PersonalConfigTranslate::class;

}