<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class CareerConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'career_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
							'title_on_image',
							'title_on_page',
							'text_on_page'
							];

    /**
     * @var bool
     */
    public $timestamps = false;

}