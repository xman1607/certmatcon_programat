<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar')->nullable();
            $table->integer('position')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('customer_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('customers_id');
            $table->unsignedInteger('language_id');

            $table->text('name')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('customers_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
        Schema::dropIfExists('customer_translate');
    }
}
