<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class AboutUsAcredit extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'about_us_acredits';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [];


    /**
     * @var
     */
    public $translationModel = AboutUsAcreditTranslate::class;

    public function getItems(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}