<?php

return [
    'title' => 'Site',

    'model' => 'Keyhunter\Administrator\Model\Settings',

    'edit_fields' => [

        'site::email' => ['type' => 'email'],

        'site::phone' => ['type' => 'text'],

        'site::phone_fix' => ['type' => 'text'],

        'site::address' => ['type' => 'text'],

        'site::facebook' => ['type' => 'text'],

        'site::map' => ['type' => 'text'],
        
        'site::title_map' => ['type' => 'text'],

        'site::zoom_map' => [
                        'type' => 'number',
                        'min' => 1,
                        'max' => 30
        ],
        
        'site::latitude' => ['type' => 'text'],

        'site::longitude' => ['type' => 'text'],
    ]
];