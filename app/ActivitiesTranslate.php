<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class ActivitiesTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'activitie_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'title',
    	'text'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}