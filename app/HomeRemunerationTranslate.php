<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class HomeRemunerationTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'home_remuneration_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'text'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}