<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeRemunerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_remunerations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('home_remuneration_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('home_remuneration_id');
            $table->unsignedInteger('language_id');

            $table->text('text')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('home_remuneration_id')->references('id')->on('home_remunerations')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_remunerations');
        Schema::dropIfExists('home_remuneration_translate');
    }
}
