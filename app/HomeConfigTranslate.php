<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class HomeConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'home_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
                            'title_on_slider',
                            'subtitle_on_slider'
						];

    /**
     * @var bool
     */
    public $timestamps = false;

}