<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bg_image')->nullable();
            $table->string('general_presentation_image1')->nullable();
            $table->string('general_presentation_image2')->nullable();
            $table->string('short_history_image')->nullable();
            $table->timestamps();
        });

        Schema::create('about_us_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('about_us_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->string('title_on_page')->nullable();
            $table->string('subtitle_on_page')->nullable();

            $table->text('home_description')->nullable();

            $table->text('general_presentation')->nullable();
            $table->text('short_history')->nullable();

            $table->text('description_missions')->nullable();
            $table->text('description_concept')->nullable();
            $table->text('description_strategy')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('about_us_config_id')->references('id')->on('about_us_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us_configs');
        Schema::dropIfExists('about_us_config_translate');
    }
}
