<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class TestimonialsTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'testimonial_translate';

    /**
     * @var array
     */
    protected $fillable = ['name', 'text'];

    /**
     * @var bool
     */
    public $timestamps = false;

}