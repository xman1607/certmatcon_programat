<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class DownloadConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'download_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
    								'title_on_page',
    								'text1',
    								'text2'
    								];


    /**
     * @var
     */
    public $translationModel = DownloadConfigTranslate::class;

}