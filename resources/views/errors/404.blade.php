<!DOCTYPE html>
<html lang="{{ $current_lang }}">
<head>
	<title>{{ $meta->getMeta('404_title') }}</title>
	<meta name="description" content="">

	<!-- meta -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<!-- links -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" href="{{ asset('assets/css/menu_footer.css') }}">

</head>
<body>

	@include('partials.header')
	
	<div class="container text-center" style="margin-top: 200px;">
		<div class="row">
			<div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>404</h1>
				<p>{{ $meta->getMeta('404_description') }}</p>
			</div>
		</div>
	</div>
	
	@include('partials.footer')

</body>
</html>
