<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class Activities extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'activities';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'title',
    	'text'
    ];


    /**
     * @var
     */
    public $translationModel = ActivitiesTranslate::class;

    public function getActivities(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}