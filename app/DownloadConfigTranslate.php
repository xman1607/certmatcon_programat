<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class DownloadConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'download_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
							'title_on_page',
							'text1',
							'text2'
							];

    /**
     * @var bool
     */
    public $timestamps = false;

}