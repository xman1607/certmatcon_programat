@extends('layout')

@section('title')
    <title>{{ $contacts_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $contacts_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/contacte.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <section class="scroll_1">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="scrl1_up_text">
                        <h2>{!! $contacts_config_data->title_on_page !!}</h2>
                    </div>
                </div>

                <div class="col-md-offset-1 col-md-5 col-sm-6 col-xs-12">
                    <div class="scrl1_left_block scrl1_block_padding">
                        
                        <h3>{!! $meta->getMeta('office') !!}</h3>

                        <h4>
                            <img src="/assets/img/contacte/locatie.png" alt="">
                            {!! settings()->getOption('site::address') !!}
                        </h4>

                        @php $del_symbols_tel = ["(", ")", " "] @endphp
                        <a href="tel:{{ str_replace($del_symbols_tel, '', settings()->getOption('site::phone_fix'))}}">
                            <img src="/assets/img/contacte/tel.png" alt="">{!! settings()->getOption('site::phone_fix') !!}
                        </a>
                        <a href="mailto:{!! settings()->getOption('site::email') !!}"><img src="/assets/img/contacte/mail.png" alt="">{!! settings()->getOption('site::email') !!}</a>

                    </div>
                </div>

                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="scrl1_right_block scrl1_block_padding">
                        
                        <form id="contact_form">
                            
                            <h4>{!! $meta->getMeta('send_a_message') !!}</h4>

                            <input type="text" name="name" placeholder="{!! $meta->getMeta('name') !!}" required minlength="3" maxlength="191">
                            <input type="text" name="phone" class="phone" placeholder="{!! $meta->getMeta('phone') !!}" required>
                            <input type="text" name="email" class="email" placeholder="{!! $meta->getMeta('email') !!}" required minlength="3" maxlength="191" >

                            <textarea name="message" placeholder="{!! $meta->getMeta('message') !!}" required></textarea>
                            
                            <p class="note_message text-center"></p>

                            <button class="send_button">{!! $meta->getMeta('send') !!}</button>

                        </form>

                    </div>
                </div>

                <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                    <div id="maps"></div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('footer_js')
    <script src="{{ asset('assets/js/maskedinput.js') }}"></script>
    <script>

        $(document).ready(function(){

            $(".phone").mask("0(99) 999-999");

            var email_validation;
            var email_validation_popup;
            //https://ruseller.com/lessons.php?rub=32&id=152
            var pattern_email = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

            var formular_note_message = $('form#contact_form .note_message');

            // Validarea cimpului email
            $('form#contact_form .email').blur(function(){

                var email_val = $(this).val();

                if(email_val.length > 0){

                    email_validation = pattern_email.test(email_val);

                    if(!email_validation){
                        

                        $(this).css({'border-bottom-collor':'#ec3938'});

                       formular_note_message.html("{!! $meta->getMeta('incorect_format_email') !!}");
                       formular_note_message.addClass('alert-danger');

                       $('.send_button').css('background', '#ccc');
                       $('.send_button').attr('disabled', true);

                    }else{
                        $(this).css({'border-bottom':'1px solid #e1e1e1'});

                        formular_note_message.empty();
                        formular_note_message.removeClass('alert-danger');

                        $('.send_button').css('background', '#d0422c');
                        $('.send_button').attr('disabled', false);
                    }

                }else{
                    email_validation = false;

                     $(this).css({'border-bottom-collor':'#ec3938'});

                    formular_note_message.html("{!! $meta->getMeta('complet_required_fields') !!}");
                    formular_note_message.addClass('alert-danger');

                    $('.send_button').css('background', '#ccc');
                    $('.send_button').attr('disabled', true);
                }
            });

            
            $('form#contact_form .send_button').click(function(event) {

                event.preventDefault();
                var form = $(this).parent('form');
                var form_data_serialize = $(this).parents('form').serialize();

                //Проверяем если заполнены все поля
                var required_filds = $(this).parents('form').find("[required]");
                var empty_filds = [];

                var send_button = $(this);

                $(required_filds).each(function(index){
                    if ( $(this).val() == '' ){

                        empty_filds[index] = $(this);

                        $(this).css('border-bottom-color', '#ec3938');
                        $(this).focus();

                        formular_note_message.html("{{ $meta->getMeta('complet_required_fields') }}");
                        
                        formular_note_message.addClass('alert-danger');

                        send_button.css('background', '#ccc');
                        send_button.attr('disable', true);

                        setTimeout(function() {

                            formular_note_message.empty();
                            formular_note_message.removeClass('alert-danger');

                            send_button.css('background', '#d0422c');
                            send_button.attr('disable', false);
                        }, 3000);

                    }else{
                        $(this).css({'border-bottom-color':'#e1e1e1'});
                    }
                });

                if(empty_filds.length == 0){

                    //daca emailul e valid
                    if(email_validation){

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '{{route('sendFormular')}}',
                            method: 'POST',
                            dataType: 'json',
                            data: form_data_serialize,

                            beforeSend: function(){
                            
                                formular_note_message.html("{!! $meta->getMeta('send_message') !!}");
                                formular_note_message.removeClass('alert-success');
                                formular_note_message.removeClass('alert-danger');
                                formular_note_message.addClass('alert-info');
                            },

                            success: function(data) {

                                if (data.result_valid == 'succes') {

                                    formular_note_message.html("{{ $meta->getMeta('message_send_success') }}");

                                    formular_note_message.removeClass('alert-info');
                                    formular_note_message.addClass('alert-success');

                                    setTimeout(function() {
                                        formular_note_message.empty();
                                        formular_note_message.removeClass('alert-success');
                                    }, 3000);

                                    form.trigger( 'reset' );


                                } else {
                                    
                                    formular_note_message.html("{{ $meta->getMeta('message_send_error') }}");
                                    formular_note_message.removeClass('alert-info');
                                    formular_note_message.addClass('alert-danger');

                                    setTimeout(function() {
                                        formular_note_message.empty();
                                        formular_note_message.removeClass('alert-danger');
                                    }, 3000);

                                }
                            },
                            async: false
                        });
                    }
                }

            });

        });

        function initMap() {

            var latitude = "{!! settings()->getOption('site::latitude') !!}";
            var longitude = "{!! settings()->getOption('site::longitude') !!}";
            var zoom_map = "{!! settings()->getOption('site::zoom_map') !!}";

            var uluru = {lat: Number(latitude), lng: Number(longitude)};
            var map = new google.maps.Map(document.getElementById('maps'), {
                zoom: Number(zoom_map),
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });

            var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            "<h4>{!! settings()->getOption('site::address') !!}</h4>"+
            '</div>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map,
                title: "{!! settings()->getOption('site::title_map') !!}"
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        }
    </script>
    <script async defer
    src="{!! settings()->getOption('site::map') !!}">

@endsection