<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class HomeSlider extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'home_sliders';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [];


    /**
     * @var
     */
    public $translationModel = HomeSliderTranslate::class;

    public function getItems(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}