@extends('layout')

@section('title')
    <title>{{ $testimonials_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $testimonials_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/testemonials.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    

    <section class="scroll_1" style="background: url({{str_replace('\\','/',$testimonials_config_data->bg_image_top)}}) center no-repeat / cover;">
        
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="scrl1_content zindex">
                        <h3>{!! $testimonials_config_data->title_on_page !!}</h3>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <section class="scroll_2">

        <div class="container">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="testemonials_slide">

                        @foreach($testimonials as $testimonial)
                            
                            <div class="slide">

                                <div class="photo_slide" style="background: url({{str_replace('\\','/',$testimonial->image)}}) center no-repeat / cover;"></div>

                                <h3>{!! $testimonial->name !!}</h3>
                                <h4>{!! $testimonial->text !!}</h4>

                            </div>

                        @endforeach
                        
                    </div>

                </div>

            </div>
        </div>

    </section>

@endsection

@section('footer_js')
    
@endsection