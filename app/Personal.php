<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class Personal extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'personals';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'name', 
    	'function'
    ];

    /**
     * @var
     */
    public $translationModel = PersonalTranslate::class;

    public function getPersonals(){
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================

    public function scopeActive($query){
        $query->where('active', 1);
    }
}