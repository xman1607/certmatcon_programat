<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::multilingual(function () {

	Route::get('/', [
		'as' => 'index',
		'uses' => 'MainController@index'
	]);

	Route::get('/about_us', [
		'as' => 'about_us',
		'uses' => 'MainController@about_us'
	]);

	Route::get('/activities', [
		'as' => 'activities',
		'uses' => 'MainController@activities'
	]);

	Route::get('/customers', [
		'as' => 'customers',
		'uses' => 'MainController@customers'
	]);

	Route::get('/testimonials', [
		'as' => 'testimonials',
		'uses' => 'MainController@testimonials'
	]);

	Route::get('/contacts', [
		'as' => 'contacts',
		'uses' => 'MainController@contacts'
	]);

	Route::get('/news', [
		'as' => 'news',
		'uses' => 'MainController@news'
	]);

	Route::get('/new/{slug_new}', [
		'as' => 'page_new',
		'uses' => 'MainController@page_new'
	]);

	Route::get('/download', [
		'as' => 'download',
		'uses' => 'MainController@download'
	]);

	Route::get('/career', [
		'as' => 'career',
		'uses' => 'MainController@career'
	]);

	Route::get('/personal', [
		'as' => 'personal',
		'uses' => 'MainController@personal'
	]);

	Route::post('/sendFormular', [
        'as' => 'sendFormular',
        'uses' => 'MainController@sendFormular'
    ]);

    Route::post('/sendFormularCustomers', [
        'as' => 'sendFormularCustomers',
        'uses' => 'MainController@sendFormularCustomers'
    ]);

    Route::post('/send_cv', [
        'as' => 'send_cv',
        'uses' => 'MainController@send_cv'
    ]);

});



