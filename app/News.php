<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class News extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'title', 
    	'meta_description',
    	'excerpt',
    	'text'
    ];


    /**
     * @var
     */
    public $translationModel = NewsTranslate::class;

    public function getNewsOnHomePage(){
        //->
        return $this->where('on_home_page', 1)->orderBy('public_date', 'desc')->active()->get();
    }

    public function getNews(){
        return $this->orderBy('public_date', 'desc')->active()->paginate(2);
    }

    //Selectam noutatea dupa link
    public function getNewOnSlug($slug_new){
        return $this->where('slug', $slug_new)->first();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }


}