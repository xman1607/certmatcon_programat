<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bg_image_top')->nullable();
            $table->timestamps();
        });

        Schema::create('testimonials_config_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('testimonials_config_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->string('title_on_page')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('testimonials_config_id')->references('id')->on('testimonials_configs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials_configs');
        Schema::dropIfExists('testimonials_config_translate');
    }
}
