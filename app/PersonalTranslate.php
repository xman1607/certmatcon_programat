<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class PersonalTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'personal_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'name', 
    	'function'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}