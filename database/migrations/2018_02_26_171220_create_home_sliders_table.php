<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('home_slider_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('home_slider_id');
            $table->unsignedInteger('language_id');

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('home_slider_id')->references('id')->on('home_sliders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_sliders');
        Schema::dropIfExists('home_slider_translate');
    }
}
