<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class HomeRemuneration extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'home_remunerations';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = [
    	'text'
    ];


    /**
     * @var
     */
    public $translationModel = HomeRemunerationTranslate::class;

    public function getRemunerations(){
        
        return $this->orderBy('position')->active()->get();
    }

    // =============================== Scope ===============================
    public function scopeActive($query){
        $query->where('active', 1);
    }

}