<?php

use App\CareerConfig;

return [
    'title'  => 'Configurarea paginii',

    'description' => 'Configurarea paginii',

    'model'  => CareerConfig::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        'title',

        'meta_description'
    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query;
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'title' => form_text('Titlul paginii in tab ') + translatable(),

        'meta_description' => form_textarea('Descrierea paginii ( Pentru SEO )') + translatable(),

        'bg_image' => [
            'label' => 'imaginea de sus',
            'type' => 'image',
            'location' => 'upload/career'
        ],

        'title_on_image' => form_text('Titlul de pe imagine') + translatable(),

        'title_on_page' => form_text('Titlul de pe pagina') + translatable(),

        'text_on_page' => form_textarea('Textul de pe pagina') + translatable(),
    ]
];