@extends('layout')

@section('title')
    <title>{{ $home_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $home_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <!-- scroll 1 -->
    <section class="scroll_1">
        
        <div class="scroll_1_bg">
            
            @foreach($home_slider as $slider)
                <div class="slide_scrl_1" style="background: url({{str_replace('\\','/',$slider->image)}})center no-repeat / cover;">
                </div>
            @endforeach

        </div>

        <div class="container">
            <div class="row">

                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="scroll_1_content zindex">

                        <h3>{!! $home_config_data->title_on_slider !!}</h3>
                        <h4>{!! $home_config_data->subtitle_on_slider !!}</h4>
                        <a href="#">{!! $meta->getMeta('view_details') !!}</a>

                    </div>
                </div>

            </div>
        </div>

    </section>

    <section class="scroll_2">
        <div class="container">
            <div class="row">
                
                <div class="col-md-5 col-sm-6">
                    <div class="scroll_2_content">
                        
                        <h3>{!! $meta->getMeta('certmatcon') !!}</h3>

                        <h4>{!! $home_about_us->home_description !!}</h4>

                        <a href="{{ route('about_us') }}">{!! $meta->getMeta('learn_more') !!}</a>

                    </div>
                </div>

                <div class="col-md-offset-1 col-md-5 col-sm-6">

                    <div class="scrl2_img" style="background: url(/assets/img/scrl2_img.jpg) center no-repeat / cover;">
                    </div>

                </div>

            </div>
        </div>
    </section>

    <section class="scroll_3">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-4 col-md-3 col-sm-12 col-xs-12">
                    <div class="scrl3_up_text">
                        <h3>
                            {!! $meta->getMeta('benefits') !!}
                        </h3>
                        <h4>
                            {!! $meta->getMeta('description_benefits') !!}
                        </h4>
                    </div>
                </div>

            </div>

            <div class="row">
                
                @foreach($home_benefits as $benefit)
                    <div 
                        @if($loop->first)
                            class="col-md-3 col-sm-6 col-xs-12"
                        @else
                            class="col-md-offset-1 col-md-3 col-sm-6 col-xs-12"
                        @endif
                    >
                        <div class="scrl3_column">

                            <div class="scrl_3_col_up_img">
                                <img src="{{ asset($benefit->image) }}" alt="">
                            </div>

                            <h3>{!! $benefit->title !!}</h3>

                            <h4>{!! $benefit->text !!}</h4>

                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>

    <section class="scroll_4">
        <div class="container">

            <div class="row">
                
                <div class="col-md-offset-4 col-md-4 col-sm-12 col-xs-12">

                    <div class="scrl_4_up_text">
                        <h3>{!! $meta->getMeta('news') !!}</h3>
                        <h4>{!! $meta->getMeta('description_news') !!}</h4>
                    </div>

                </div>

            </div>

            <div class="row">
                
                @foreach($home_news as $new)
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{ $new->slug }}" class="scrl4_col">

                            <div class="scrl4_col_up_img" style="background: url({{str_replace('\\','/',$new->image_preview)}}) center no-repeat / cover;">
                            </div>

                            <h4>{{ $new->public_date }}</h4>
                            <h3>{!! $new->title !!}</h3>

                        </a>
                    </div>
                @endforeach

            </div>

        </div>
    </section>

    <section class="scroll_5">
        <div class="container">
            <div class="row">
                
                @foreach($acreditari as $acredit)
                    
                    <div 
                        @if($loop->first)
                            class="col-md-offset-3 col-md-3 col-sm-6 col-xs-12"
                        @else
                            class="col-md-3 col-sm-6 col-xs-12"
                        @endif
                    >
                        
                        <div 
                            @if($loop->iteration % 2 != 0)
                                class="scrl5_column scrl5_columnL"
                            @else
                                class="scrl5_column scrl5_columnR"
                            @endif
                        >
                            <div class="scrl_5_col_img">
                                <img src="{{ asset($acredit->image) }}" alt="">
                            </div>
                            <h3>{!! $acredit->text !!}</h3>
                        </div>

                    </div>

                @endforeach
            </div>
        </div>
    </section>

@endsection

@section('footer_js')

@endsection