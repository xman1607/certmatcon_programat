<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar')->nullable();
            $table->string('fax')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->integer('position')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('personal_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('personal_id');
            $table->unsignedInteger('language_id');

            $table->string('name')->nullable();
            $table->string('function')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('personal_id')->references('id')->on('personals')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personals');
        Schema::dropIfExists('personal_translate');
    }
}
