<?php

use App\AboutUsConfig;

return [
    'title'  => 'Configurarea paginii',

    'description' => 'Configurarea paginii',

    'model'  => AboutUsConfig::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        'title',

        'meta_description'
    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query;
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'title' => form_text('Titlul paginii in tab ') + translatable(),

        'meta_description' => form_textarea('Descrierea paginii ( Pentru SEO )') + translatable(),

        'home_description' => form_textarea('Descrierea de pe pagina principala') + translatable(),

        'title_on_page' => form_text('Titlul de pe imaginea principala') + translatable(),

        'subtitle_on_page' => form_text('Subtitlu de pe imaginea principala') + translatable(),

        'bg_image' => [
            'label' => 'Imaginea principala a paginii',
            'type' => 'image',
            'location' => 'upload/about_us'
        ],

        'general_presentation' => form_textarea('Prezentare generală') + translatable(),

        'general_presentation_image1' => [
            'label' => 'Prima imagine de la prezentarea generală',
            'type' => 'image',
            'location' => 'upload/about_us'
        ],

        'general_presentation_image2' => [
            'label' => 'A doua imagine de la prezentarea generală',
            'type' => 'image',
            'location' => 'upload/about_us'
        ],

        'short_history' => form_textarea('Scurt istoric') + translatable(),

        'short_history_image' => [
            'label' => 'Imaginea de fundal de la Scurt istoric',
            'type' => 'image',
            'location' => 'upload/about_us'
        ],

        'description_missions' => form_textarea('Descriere Misiune') + translatable(),

        'description_concept' => form_textarea('Descriere Concept') + translatable(),

        'description_strategy' => form_textarea('Descriere Strategie') + translatable(),

    ]
];