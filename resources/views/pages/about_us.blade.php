@extends('layout')

@section('title')
    <title>{{ $about_us_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $about_us_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/despre_certmatcon.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <section class="scroll_1" style="background: url({{str_replace('\\','/',$about_us_config_data->bg_image)}}) center no-repeat / cover;">
        <div class="container">
            <div class="row">
                
                <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="scrl1_content">
                        <h3>{!! $about_us_config_data->title_on_page !!}</h3>
                        <h4>{!! $about_us_config_data->subtitle_on_page !!}</h4>
                        <a href="#">{!! $meta->getMeta('view_details') !!}</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="scroll_2">
        
        <div class="container">
            <div class="row">
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="scrl_2_img_block" style="background: url({{str_replace('\\','/',$about_us_config_data->general_presentation_image1)}}) center no-repeat / cover;"></div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="scrl_2_img_block" style="background: url({{str_replace('\\','/',$about_us_config_data->general_presentation_image2)}}) center no-repeat / cover;"></div>
                </div>


                <div class="col-md-offset-1 col-md-5 col-sm-12 col-xs-12 no-padding">

                    <div class="scrl2_text_content">
                        <h2>{!! $meta->getMeta('general_presentation') !!}</h2>
                        <p id="text_general_presentation" style="padding-bottom: 0; display: inline;">
                            {!! substr($about_us_config_data->general_presentation, 0, 300) !!}<span class="continue_points">...</span></p><p id="text_general_presentation_full" style="display: none; display: none;">{!! substr($about_us_config_data->general_presentation, 300) !!}
                        </p>

                        <a href="javascript:void(0);" style="display: block;" id="link_read_more" data-title="show" >{!! $meta->getMeta('read_more') !!}</a>
                        
                    </div>

                </div>

            </div>
        </div>

    </section>

    <section class="scroll_3" style="background: url({{str_replace('\\','/',$about_us_config_data->short_history_image)}}) center no-repeat / cover;">
        
        <div class="container">
            <div class="row">
                
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="scrl3_content">
                        
                        <h3>{!! $meta->getMeta('short_history') !!}</h3>
                        <p>{!! $about_us_config_data->short_history !!}</p>

                        </div>
                    </div>

                </div>
            </div>

        </section>

        <section class="scroll_4">
            <div class="container">
                <div class="row">

                    <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">

                        <div class="scrl4_up_text">
                            <h3>{!! $meta->getMeta('mission_concept_strategy') !!}</h3>
                            <p>{!! $meta->getMeta('mission_concept_strategy_description') !!}</p>
                        </div>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="scrl4_column_parent">

                            <div class="scrl4_column">
                                
                                <img src="/assets/img/despre_certmatcon/scrl4_img1.png" alt="">
                                <hr>

                                <h2>{!! $meta->getMeta('mission') !!}</h2>
                                <p>{!! $about_us_config_data->description_missions !!}</p>

                            </div>

                            <div class="scrl4_column">
                                
                                <img src="/assets/img/despre_certmatcon/scrl4_img2.png" alt="">
                                <hr>

                                <h2>{!! $meta->getMeta('concept') !!}</h2>
                                <p>{!! $about_us_config_data->description_concept !!}</p>

                            </div>

                            <div class="scrl4_column">
                                
                                <img src="/assets/img/despre_certmatcon/scrl4_img3.png" alt="">
                                <hr>

                                <h2>{!! $meta->getMeta('strategy') !!}</h2>
                                <p>{!! $about_us_config_data->description_strategy !!}</p>

                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </section>

        <section class="scroll_5">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="scrl5_up_text">
                            <h4>{!! $meta->getMeta('accreditations') !!}</h4>
                        </div>
                    </div>
                    
                    <div class="col-md-offset-1 col-md-10">
                        @if(count($acredits) > 0)
                            <div class="slick_depsre_certmatcon">
                                @foreach($acredits as $acredit)
                                    <div class="slide">
                                        <img src="{{ asset( $acredit->image ) }}" alt="">
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <h3 class="text-center">
                                {!! $meta->getMeta('no_accreditations') !!}
                            </h3>
                        @endif
                    </div>

                </div>
            </div>
        </section>

@endsection

@section('footer_js')
    
<script type="text/javascript">
    
    $(document).ready(function(){

        $('#link_read_more').click(function(){

            if($('#text_general_presentation_full').css("display")=="inline"){
                $('#text_general_presentation_full').css("display", "none");
                $('#text_general_presentation span.continue_points').css('display', 'inline');
            }else{
                $('#text_general_presentation_full').css("display", "inline");
                $('#text_general_presentation span.continue_points').css('display', 'none');
            }


            if($(this).data('title') == 'show'){
                $(this).data('title', 'hide');
                $(this).text("{!! $meta->getMeta('read_less') !!}");
            }else{
                $(this).data('title', 'show');
                $(this).text("{!! $meta->getMeta('read_more') !!}");
            }
        });
    });

</script>

@endsection