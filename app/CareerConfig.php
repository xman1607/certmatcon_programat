<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class CareerConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'career_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
    								'title_on_image',
    								'title_on_page',
    								'text_on_page'
    								];


    /**
     * @var
     */
    public $translationModel = CareerConfigTranslate::class;

}