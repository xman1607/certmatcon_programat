<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class AboutUsConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'about_us_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 
    								'meta_description',
                                    'title_on_page',
                                    'subtitle_on_page',
                                    'home_description',
                                    'general_presentation',
                                    'short_history',
                                    'description_missions',
                                    'description_concept',
                                    'description_strategy'
    							];


    /**
     * @var
     */
    public $translationModel = AboutUsConfigTranslate::class;

}