<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class NewsTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'new_translate';

    /**
     * @var array
     */
    protected $fillable = [
    	'title', 
    	'meta_description',
    	'excerpt',
    	'text'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

}