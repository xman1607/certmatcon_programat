@extends('layout')

@section('title')
    <title>{{ $new_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $new_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/noutati_interior.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <div class="scroll_1">

        <div class="container">
            <div class="row">

                <div class="scrl1_up_text">
                    <h3>{!! $meta->getMeta('new_news') !!}</h3>
                </div>

                <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">

                    <div class="article_block">

                        <div class="ab_up_text">
                            <h2>
                                {!! $new_data->title !!}
                                <span>
                                    <img src="{{ asset('assets/img/noutati_interior/line_article.png') }}" alt="">
                                </span>
                            </h2>
                            
                            {!! $new_data->text !!}

                            <p>
                                {!! $meta->getMeta('new_sign') !!}
                            </p>
            
                        </div>

                    </div>

                    <div class="share_block">
                        <h4>{!! $meta->getMeta('share') !!}</h4>

                        <a href="https://www.facebook.com/sharer/sharer.php?u=http://<?=$_SERVER['SERVER_NAME']?>/new/{{ $new_data->slug }}" onclick="window.open(this.href,'targetWindow','menubar=no,scrollbars=yes,resizable=yes,width=660,height=600');return false;">
                            <img src="{{ asset('assets/img/noutati_interior/share_1.png') }}" alt="">
                        </a>

                        <a href="https://twitter.com/share?url=http://<?=$_SERVER['SERVER_NAME']?>/new/{{ $new_data->slug }}" onclick="window.open(this.href,'targetWindow','menubar=no,scrollbars=yes,resizable=yes,width=660,height=600');return false;">
                            <img src="{{ asset('assets/img/noutati_interior/share_2.png') }}" alt="">
                        </a>

                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://<?=$_SERVER['SERVER_NAME']?>/new/{{ $new_data->slug }}" onclick="window.open(this.href,'targetWindow','menubar=no,scrollbars=yes,resizable=yes,width=660,height=600');return false;">
                            <img src="{{ asset('assets/img/noutati_interior/share_3.png') }}" alt="">
                        </a>

                        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                            <img src="{{ asset('assets/img/noutati_interior/share_4.png') }}" alt="">
                        </a>

                        <a href="https://plus.google.com/share?url=http://<?=$_SERVER['SERVER_NAME']?>/new/{{ $new_data->slug }}" onclick="window.open(this.href,'targetWindow','menubar=no,scrollbars=yes,resizable=yes,width=660,height=600');return false;">
                            <img src="{{ asset('assets/img/noutati_interior/share_5.png') }}" alt="">
                        </a>

                    </div>

                    <div class="visible_block">
                        <h4>{{ $new_data->public_date }}</h4>
                        <!-- <h4>{{ $new_data->views }}</h4> -->
                        <hr>
                    </div>

                    <div class="facebook_coments">
                        <div class="fb-comments" data-href="http://<?=$_SERVER['SERVER_NAME']?>/new/{{ $new_data->slug }}" data-width="100%" data-numposts="5"></div>
                    </div>

                    </div>

                </div>
            </div>

        </div>
        
        <div id="fb-root"></div>
@endsection

@section('footer_js')
    
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

@endsection