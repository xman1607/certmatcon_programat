<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_preview')->nullable();
            $table->string('slug')->nullable();
            $table->date('public_date')->nullable();

            //$table->integer('position')->default(1);
            $table->boolean('active')->default(true);
            $table->boolean('on_home_page')->default(true);
            $table->timestamps();
        });

        Schema::create('new_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('language_id');

            $table->string('title')->nullable();
            $table->text('meta_description')->nullable();

            $table->text('excerpt')->nullable();
            $table->longText('text')->nullable();

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('new_translate');
    }
}
