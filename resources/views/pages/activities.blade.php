@extends('layout')

@section('title')
    <title>{{ $activities_config_data->title }}</title>
@endsection

@section('description')
    <meta name="description" content="{{ $activities_config_data->meta_description }}">
@endsection

@section('assets_css')
    <link rel="stylesheet" href="{{ asset('assets/css/activatii.css') }}">
@endsection

@section('assets_js')

@endsection


@section('content')
    
    <div class="scroll_1">
        <div class="container">
            <div class="row">

                <div class="scrl_1_up_text">
                    <h3>{!! $activities_config_data->title_on_page !!}</h3>
                </div>
                
                <div class="col-md-4 col-sm-4 col-xs-12">
                    
                    <div class="left_block">

                        <ul>
                            
                            @foreach( $activities as $activity )
                                
                                <li
                                    @if($loop->first)
                                        class="active"
                                    @endif
                                >
                                    <a data-toggle="tab" href="#content{{ $loop->iteration }}">
                                        {!! $activity->title !!}
                                    </a>
                                </li>

                            @endforeach
                        </ul>

                        <span></span>

                    </div>

                </div>

                <div class="col-md-7 col-sm-8 col-xs-12">
                    
                    <div class="tab-content">

                        @foreach( $activities as $activity )
                            
                            <div 
                                @if($loop->first)
                                    class="tab_content_activatii tab-pane in active"
                                @else
                                    class="tab_content_activatii tab-pane"
                                @endif

                                id="content{{ $loop->iteration }}"
                            >

                                <p>
                                    {!! $activity->text !!}
                                </p>

                                <div class="tab_content_activatii_img" style="background: url({{str_replace('\\','/',$activity->image)}}) center no-repeat / cover;"></div>

                            </div>
                            
                        @endforeach

                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection

@section('footer_js')

@endsection