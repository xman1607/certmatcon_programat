<?php

use App\Testimonials;

return [
    'title'  => 'Testimoniale',

    'description' => 'Testimoniale',

    'model'  => Testimonials::class,

    /*
    |-------------------------------------------------------
    | Columns/Groups
    |-------------------------------------------------------
    |
    | Describe here full list of columns that should be presented
    | on main listing page
    |
    */
    'columns' => [
        'id',

        'position',

        'image' => column_element('', false, '<img width="100" src="(:image)" />'),

        'name',

        'text',

        'active' => column_element('Activ', false, function($row)
        {
            return output_boolean($row);
        }),
    ],

    /*
    |-------------------------------------------------------
    | Actions available to do, including global
    |-------------------------------------------------------
    |
    | Global actions
    |
    */
    'actions' => [

    ],

    /*
    |-------------------------------------------------------
    | Eloquent With Section
    |-------------------------------------------------------
    |
    | Eloquent lazy data loading, just list relations that should be preloaded
    |
    */
    'with' => [

    ],

    /*
    |-------------------------------------------------------
    | QueryBuilder
    |-------------------------------------------------------
    |
    | Extend the main scaffold index query
    |
    */
    'query' => function($query)
    {
        return $query->orderBy('position');
    },

    /*
    |-------------------------------------------------------
    | Global filter
    |-------------------------------------------------------
    |
    | Filters should be defined here
    |
    */
    'filters' => [

    ],

    /*
    |-------------------------------------------------------
    | Editable area
    |-------------------------------------------------------
    |
    | Describe here all fields that should be editable
    |
    */
    'edit_fields' => [

        'id'       => form_key(),

        'name' => form_text('Nume Prenume') + translatable(),

        'text' => form_textarea('Textul') + translatable(),

        'image'       => [
            'label' => 'Imaginea',
            'type'=> 'image',
            'location'=> 'upload/testimonials/avatars'
        ],

        'position'    => form_number($label = 'Ordinea. Numar intreg. Cu cit e mai mare, cu atit e mai la dreapta.', ['min' => 1, 'max' => 100000, 'required' => 'required']),

        'active' => filter_select('Activ', [
            1 => 'Da',
            0 => 'Nu'
        ]),

    ]
];