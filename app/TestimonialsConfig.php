<?php
namespace  App;

use Keyhunter\Administrator\Repository;
use Keyhunter\Translatable\HasTranslations;

/**
 * Class Condition
 * @package App
 */
class TestimonialsConfig extends Repository {

    use HasTranslations;

    /**
     * @var string
     */
    protected $table = 'testimonials_configs';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $translatedAttributes = ['title', 'meta_description', 'title_on_page'];


    /**
     * @var
     */
    public $translationModel = TestimonialsConfigTranslate::class;


}