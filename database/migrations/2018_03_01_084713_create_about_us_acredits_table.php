<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutUsAcreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_us_acredits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('position')->default(1);;
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::create('about_us_acredit_translate', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('about_us_acredit_id');
            $table->unsignedInteger('language_id');

            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('about_us_acredit_id')->references('id')->on('about_us_acredits')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_us_acredits');
        Schema::dropIfExists('about_us_acredit_translate');
    }
}
