<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class PersonalConfigTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'personal_config_translate';

    /**
     * @var array
     */
    protected $fillable = ['title', 
							'meta_description',
							'title_on_image',
							'subtitle_on_image',
							'title_on_page'
							];

    /**
     * @var bool
     */
    public $timestamps = false;

}