<?php
namespace  App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ConditionTranslate
 * @package App
 */
class AboutUsAcreditTranslate extends Model {

    /**
     * @var string
     */
    protected $table = 'about_us_acredit_translate';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @var bool
     */
    public $timestamps = false;

}